// create a new server that handles requests for user routes. The following feature must be included:
/*
1. Get a list of all users.
2. view a specific user's profile
3. update user information
4. delete a user
5. register a new user.
*/

// create a new Postman collection called "Node.js Activity 1" and add all the routes to that collection. Make sure to test each one, and then export the collection to your activity folder.

let http = require("http")

const server = http.createServer(function(request, response){

	if(request.url === '/api/users' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("List all of the users from database")

	}else if(request.url === '/api/users/5647' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User1 profile showing from database")

	}else if(request.url === '/api/users/5647' && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Updated User1 profile.")	

	}else if(request.url === '/api/users/5647' && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("User1 profile deleted.")	
			
	}else if(request.url === '/api/users' && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Added a new user to the database")
				
	}
})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)