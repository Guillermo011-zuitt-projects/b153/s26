// Introduction to Postman and REST

/*
When a user makes a request, the url endpoint is not the only property included in that request. There is also an HTTP method that helps determine the exact oeprations for the server to accomplish.

The four HTTP methods are as follows:

GET - retrieve information about a resource
	- by default, whenever a browser loads any page, it makes a GET request
POST - used to create a new resource
PUT - used to update information about an already-existing resource
DELTE - used to delete an already-existing resource

*/

/*
let http = require("http")

const server = http.createServer(function(request, response){
	// console.log(request.method)

if(request.method === "GET" && request.url === "/check"){
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end("Your request is a GET request")
}else{
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end("Hello World!")
}
})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)
*/

/*
Scenario: You're using a website for information about music called www.coolmusicdb.com
The first thing you want to see is a list of all the songs on the website.
You got to www.coolmusicdb.com/api/songs - the browsers is able to read both the URL endpoint of the address. (/api/songs) that you provided as well as the method of request that you are making.
Since we are simply loading a page on a browser, the request that you send is automatically sent as a GET request.
The browser accepts your request, and creates the request object with the following information (and more):
{
	url: "api/songs",
	method: "GET"
}
and then shows you its entire list of songs.
Next, you want to see specific information about one song. For eaxample "Happy Birthday to You"
You click the link for that song, and it takes you to www.coolmusicdb.com/api/songs/5464123135.

The reqeust object for this looks like:
{
	url: "api/songs/5464123135",
	method: "GET"
}
Next you want to add your own original song to the databse. You fill out the forms, and then press the submit button.
The broswer makes a request that looks like this:
{
	url: "api/songs",
	method: "POST"
}
{
	url: "api/songs",
	method: "PUT"
}
Finally, you decide to remove your song in the list.
{
	url: "api/songs/5464123135",
	method: "DELETE"
}
*/


let http = require("http")

const server = http.createServer(function(request, response){

	if(request.url === '/api/products' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Products retrieved from database.")

	}else if(request.url === '/api/products/123' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Product 123 retrieved from database.")

	}else if(request.url === '/api/products/123' && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Product 123 updated.")	

	}else if(request.url === '/api/products/123' && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Product 123 deleted.")	
			
	}else if(request.url === '/api/products' && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("New product added to database.")
				
	}
})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)
